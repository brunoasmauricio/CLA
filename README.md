# C Programming language

This repository holds tools and other resources that will help you in understanding how the C programming language works.

## Getting started

These resources are part of the [ISEP Academy CLA course](https://www.isepacademy.isep.ipp.pt/) and are auxiliary to the main course.

## Tools

### Logic debugger

The tool in tools/debug_logic allows you to run your program line by line, all the while seeing how the variables and memory changes.

See more about this in tools/debug_logic/README.md

## TODO

[ ] Add appropriate link in README when course launches
