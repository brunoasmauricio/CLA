#include <stddef.h>
#include <stdlib.h>

extern void *__real_malloc(size_t size);
extern void *__real_calloc(size_t nmemb, size_t size);
extern void *__real_realloc(void * ptr, size_t size);
extern void *__real_reallocarray(void * ptr, size_t nmemb, size_t size);


void *__wrap_malloc(size_t size) {
    void* ret_ptr = __real_malloc(size);
    return ret_ptr;
}

void *__wrap_calloc(size_t nmemb, size_t size) {
    void* ret_ptr = __real_calloc(nmemb, size);
    return ret_ptr;
}

void *__wrap_realloc(void * ptr, size_t size) {
    void* ret_ptr = __real_realloc(ptr, size);
    return ret_ptr;
}

void *__wrap_reallocarray(void * ptr, size_t nmemb, size_t size) {
    void* ret_ptr = __real_reallocarray(ptr, nmemb, size);
    return ret_ptr;
}

