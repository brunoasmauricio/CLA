#include <stdio.h>

double findMedianSortedArrays(int* W, int WSize, int* F, int FSize){
    int wi = 0, fi = 0;
    int t = (WSize + FSize) / 2;

    while (wi + fi < t) {
        while(W[wi] < F[fi] && wi < WSize) {
            wi++;
        }
        while(W[wi] >= F[fi] && fi < FSize) {
            fi++;
        }
    }

    if ((WSize + FSize) % 2 == 0){
        if(W[wi] < F[fi]) {
            return W[wi];
        }
        return F[fi];
    }
    int a, b;
    if (W[wi] < F[fi]) {
        a = W[wi++];
    } else {
        a = F[fi++];
    }
    
    if (W[wi] < F[fi]) {
        b = W[wi++];
    } else {
        b = F[fi++];
    }
    return (a + b) / 2;
}

int main() {
    int a[] = {1, 3};
    int b[] = {2};
    printf("TELL ME SOMETHING %d\n", findMedianSortedArrays(a, 2, b, 1));
}

/*
#include <stdio.h>
#include <stdlib.h>

static int d[4][5];
static int b[4];
int v = 4;
int f(int a[3]) {
    int d = 39;
    int b = 5;
    char* c = (char*)malloc(sizeof(char) * 3);
    for(int i = 0; i < 3; i++) {
        // c[i] = c[2 - i] * 3;
        // a[i] = c[2 - i];
        a[i] = 15;
        c[i] = 15;
        v += a[i];
    }
    return v + a[1] + d;
}


int main() {
    int a[3] = {3, 3, 3};
    int b[3] = {0x6578, 0x6578, 0x6578};
    int c[3] = {0xabd31, 0xabd31, 0xabd31};
    int d[3][2] = {{1,2}, {3,4}, {5,6}};
    printf("%d\n", f(a));
    printf("%d\n", f(b));
    printf("%d\n", f(c));

    d[2][1] = 43;
    d[1][2] = 64;
}
*/

