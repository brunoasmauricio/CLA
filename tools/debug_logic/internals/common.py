import gdb

from colorama import Fore, Style
import unicodedata

#       Formatting
def format_update(old, new):
    return Fore.BLUE+old+Fore.YELLOW+" => "+Fore.GREEN+new+Style.RESET_ALL

def format_new(data):
    return Fore.YELLOW+data+Style.RESET_ALL

def remove_control_characters(s):
    return "".join(ch for ch in s if unicodedata.category(ch)[0] != "C" or ch == "\n" or ch == "\t")


#       Output
# DEBUG = True
DEBUG = False

fifo_path = str(gdb.parse_and_eval("$fifo_path"))
fifo_path = fifo_path[1:-1] # remove trailing '"'s

# Print to file pager
def print(to_print):
    if to_print == "":
        return
    with open(fifo_path, 'a') as fp:
        fp.write(str(to_print))
        fp.write("\n")

# Only print if DEBUG == True
def debug(to_print):
    if DEBUG == True:
        print(to_print)
