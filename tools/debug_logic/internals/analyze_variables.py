import gdb
from time import sleep
import os,sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from gdb_ops import *
from common import *

# Variable state
variable_map ={}

scope_change_message = format_new("nome mudou de scope: \n")

# Update variable values
def update_variables(variables):
    global variable_map

    delta_var = []
    debug(variables)
    for var in variables.keys():
        debug(var)
        # New memory address and value
        if var not in variable_map.keys():
            variable_map[var] = variables[var]
            # Variable definition changed between value and array
            if type(variables[var]) == type([]):
                delta_var.append("Novo! "+format_new(var+" = {"))
                delta_var +=  [format_new(x) for x in variables[var]]
                delta_var.append(format_new("}"))
            else:
                delta_var.append("Novo! "+format_new(var+" = " + variables[var]))

        #Variables changed!
        elif variable_map[var] != variables[var]:

            if type(variables[var]) == type([]):
                # Variable definition changed between array and value
                if type(variable_map[var]) == type([]):
                    delta_var.append(var + " = {")
                    for entry_ind in range(len(variables[var])):
                        curr_value = variables[var][entry_ind]

                        prev_value = variable_map[var][entry_ind]

                        if curr_value != prev_value:
                            delta_var.append(format_update(prev_value, curr_value))
                        else:
                            delta_var.append(curr_value)
                    delta_var.append("}")
                else:
                    delta_var.append(scope_change_message+ var + " = {")
                    for entry_ind in range(len(variables[var])):
                        curr_value = variables[var][entry_ind]
                        delta_var.append(curr_value)
                    delta_var.append("}")

            else:
                # Variable definition changed between array and value
                if type(variable_map[var]) == type([]):
                    delta_var.append(scope_change_message+var+format_update("", variables[var]))
                else:
                    delta_var.append(var+" = "+format_update(variable_map[var], variables[var]))
        else:
            continue
        # Leave 1 empty line between vars
        delta_var.append("")

        variable_map[var] = variables[var]
    if len(delta_var) != 0:
        sleep(0.01) # Necessary of python printing to the pipe gets mangled
        print('\n'.join(delta_var))

# Look into arguments/local and global variables
def analyze_variables():
    all_vars = []
    # TODO Dont look into globals from other files
    all_vars += get_global_variables()
    all_vars += get_local_and_arg_variables()

    current_values = {}
    for var in all_vars:
        #print(var)
        current_values[var] = get_var_data(var)

    update_variables(current_values)