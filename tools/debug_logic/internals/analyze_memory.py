import gdb

import os,sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from gdb_ops import *
from common import *

# Memory state
memory_map = {}
sp_top = int(gdb.parse_and_eval("$sp_top"))
changed_func = False

# Update memory map (return data that changed in string format)
def update_memory_map(new_memory_data):
    global memory_map
    delta_mem = ""
    for addr in new_memory_data.keys():
        debug(addr)
        
        # New memory address and value
        if addr not in memory_map.keys():
            memory_map[addr] = new_memory_data[addr]

        # Memory changed
        elif memory_map[addr] != new_memory_data[addr]:
            delta_mem += "["+hex(addr)+"] "+format_update(memory_map[addr], new_memory_data[addr]) + "\n"
            memory_map[addr] = new_memory_data[addr]

    return delta_mem

# Parse memory from gdb into a dict ("address": value)
def parse_memory(memory_str):
    memory_lines = memory_str.split("\n")
    new_memory = {}
    debug(memory_lines)
    for line in memory_lines:
        # 0x7fffffffdab0:	0x00000001	0x00000000	0xf7c27cd0	0x00007fff
        debug(line)

        if line == "":
            continue

        split_line = line.replace("\t"," ").split(" ")

        debug (split_line)
        base_addr = int(split_line[0][:-1], 16)# remove :
        values = split_line[1:]
        for value in values:
            new_memory[base_addr] = value
            base_addr += 4

    debug("new memory map")
    debug(new_memory)

    return new_memory

#               STACK

# Look for changes in stack memory (obtained by comparing stack pointer values)
def analyze_stack():
    current_sp = get_reg("sp")
    min_sp = int(gdb.parse_and_eval("$min_sp"))
    prev_sp = int(gdb.parse_and_eval("$prev_sp"))

    # sp changed? We probably changed function
    global changed_func
    if prev_sp != current_sp:
        changed_func = True
        gdb.set_convenience_variable("prev_sp", current_sp)

    # SP grows downwards, always store stack limits (aka user accessible memory)
    if current_sp < min_sp:
        gdb.set_convenience_variable("min_sp", current_sp)

    debug("SP difference "+hex(sp_top - min_sp))

    stack_mem = get_memory(min_sp, sp_top - min_sp)
    stack_new_mem = parse_memory(stack_mem)
    stack_delta_mem = update_memory_map(stack_new_mem)

    # Function changed, ignore stuff compiler pushed onto the stack in prologue
    if changed_func == False:
        print(stack_delta_mem)

    changed_func = False

#               HEAP

# ptr,size pairs
allocated_ptrs = {}

# Called by an allocator function with the requested size and provided address
def save_allocation(addr, size):
    try:
        addr = addr.split(" ")[-1].replace("\n", "")
        size = size.split(" ")[-1].replace("\n", "")

        addr = int(addr, 16)
        size = int(size)

        allocated_ptrs[addr] = size
    except Exception as ex:
        print("COULD NOT SAVE ALLOCATION "+str(addr)+" ["+str(size)+"]")
        print(str(ex))

# Look into the heap memory for changes (obtained by wrapping common allocators)
def analyze_heap():
    for addr in allocated_ptrs.keys():
        heap_new_mem = get_memory(addr, allocated_ptrs[addr])
        print(update_memory_map(heap_new_mem))



# Look into heap and memory changes
def analyze_memory():
    analyze_stack()
    analyze_heap()