#! /usr/bin/python

import gdb

# Auto scroll
gdb.execute("set pagination off")
# Dont confirm exit
gdb.execute("set confirm off")
# Dont write to log file
gdb.execute("set logging enabled off")
# Source + Console layout
gdb.execute("layout src")
gdb.execute("set debuginfod enabled off")

# Silently go to main
gdb.execute("break *main")
gdb.execute("""command
silent
end
""")
gdb.execute("run")

# Set stack addresses
gdb.execute("set $sp_top=$sp")
gdb.execute("set $min_sp=$sp")
gdb.execute("set $prev_sp=$sp")

# On program end, exit debugger
gdb.execute("set $_exitcode = -999")
gdb.execute("""define hook-stop
    if $_exitcode != -999
        quit
    end
end
""")

# Allocator wrappers for heap interception
allocators = ["__wrap_malloc","__wrap_calloc","__wrap_realloc","__wrap_reallocarray"]

# On malloc, save pointer and size
for allocator in allocators:
    gdb.execute("""break """+allocator+"""
command
silent
source allocator.py
end
""")

# Setup forward command to call fstep script
gdb.execute("""define forward
source internals/fstep.py
end
""")

# Print to debugger console
print("""
Se um terminal novo com instrucoes nao aparecer, reinicia o programa ou reporta isso!
""")

# some bs so we can import from local directory without creating modules
import os,sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from time import sleep
from common import *


# Wait for gdb to load
sleep(0.1)
banner="""

                                Bem vindos!
Para avancarem pelo vosso programa, escrevam o comando "forward" e primam enter.
Apos correr uma vez o comando, basta carregar enter outra vez para o repetir

Sempre que variaveis, memoria principal do programa ou memoria de mallocs
alterarem, o terminal de suporte (onde lerem isto) que apareceu vai dizer onde,
e quais as alteracoes!
Dicas:
    1. Podem fazer scroll no painel acima;

    2. Vai aparecer algum texto nesta janela. A menos que seja um erro, ignorem;

    3. Se fizerem codigo muito manhoso pode ser que o terminal de suporte
        nao apanhe as alteracoes. Caso isso aconteca avisem;

    4. Para sair escrevam "q", seguido de um enter;

    5. Se meterem outros comandos que nao "forward" estao por vossa conta.
        Mas podem sempre pedir auxilio ou entao recomecar o programa com "q"!

    6. E normal verem a mesma alteracao tanto em memoria como variaveis.
        A menos que voces precisem de usar o endereco de uma variavel, o
        compilador pode escolher nao a guardar em memoria, so nos registos do 
        CPU

Divirtam-se :)
"""

# print banner to new terminal pager
print(banner)

# Refresh layout
gdb.execute("refresh")



