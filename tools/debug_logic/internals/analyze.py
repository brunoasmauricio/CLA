import gdb

import os,sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from analyze_memory import *
from analyze_variables import *


def loop():
    analyze_variables()
    analyze_memory()

    gdb.execute("refresh")
