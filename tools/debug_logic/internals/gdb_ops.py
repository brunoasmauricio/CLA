import gdb

import os,sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from common import *

# Obtain current integer value of register "reg_name"
def get_reg(reg_name):
    line = gdb.execute("i r "+reg_name, to_string=True)
    line = " ".join(line.split()) # Clear random spaces
    blink = line.split(" ")[1]
    return int(blink, 16)

# Obtain the values of memory at start_addr, sized start_addr (size rounded
#  up to nearest multiple of 4
def get_memory(start_addr, size):
    start_addr = hex(start_addr)
    debug("get memory at "+start_addr+", sized "+str(size))
    if size % 4 != 0:
        size += 4 - (size % 4)
    size = size/4
    size = int(str(int(size))) # Remove .0

    debug("x/"+str(size)+" "+start_addr)
    
    # Use big endian so memory """makes more sense"""
    gdb.execute("set endian big", to_string=True)
    memory = gdb.execute("x/"+str(size)+"x "+start_addr, to_string=True)
    gdb.execute("set endian auto", to_string=True)

    return memory

def get_global_variables():
    vars = []
    all_variables = gdb.execute("info variables", to_string=True).split("\n")
    for line_ind in range(len(all_variables)):
        if "File " in all_variables[line_ind]:
            for vars_ind in range(line_ind + 1, len(all_variables)):
                var_entry = all_variables[vars_ind]

                if var_entry == "":
                    break

                # Remove type info
                var_entry = var_entry.split(" ")[-1]

                # Remove ;
                var_entry = var_entry[:-1]

                # Its an array. remove [X]
                if "[" in var_entry:
                    var_entry = var_entry.split("[")[0]

                vars.append(var_entry)
            break
    return vars

def get_local_and_arg_variables():
    locals = []
    # locals
    all_locals = gdb.execute("info locals", to_string=True).split("\n")
    # args
    all_locals = all_locals + gdb.execute("info args", to_string=True).split("\n")
    for local in all_locals:
        if "=" not in local or "[" in local:
            continue
        locals.append(local.split(" ")[0])
    return locals


def get_var_data(var_name):
    value = remove_control_characters(gdb.execute("p "+var_name, to_string=True))
    if "{" in value:
        # Remove { and }
        value = value.split("\n")[1:-2]
        value = [x.replace(",","") for x in value]
        return value

    # Get hexa
    if "*" in value or "Cannot access memory at" in value or "\"" in value:
        value = gdb.execute("p/x "+var_name, to_string=True)

    value = value.replace("\n","").split(" =")[1]

    return value
