# Logic debugger

Debug the logic in your code with live memory and variable change detection!

## Running

To run the logic debugger, provide it the C source files you want to analyze!

```shell
./run.sh some_source_file_1.c some_source_file_2.c
```

A window will pop up with some tips. In this window, you will see the memory/variable changes as your program proceeds.

## Design

This set of scritps compiles the provided .c files into an executable.

The compilation is done with flags that will help in debugging.

Namely:
1. The usual '-O0' and '-g3' for better 'C source' --> 'assembly' relation

2. Some '-Wl,-wrap' to wrap the heap allocating functions

3. Some object files to simplify some more complex operations (such as file operations)

The executable is launched by the gdb debugger, and some setup is performed with the help of a Python script.

The "forward" gdb command is created, which steps through the code, launching some Python that will look into the available variables and memory for changes.

If changes are found, they are output to the terminal that spawned when run.sh was launched.


## TODO

[ ] Add detection of memory corruption

```text
Check if the bytes following an allocation are wrongly altered (but dont belong to another allocation)
Can be done by also wrapping free, or asking gcc to insert appropriate code
```

[ ] Redirect process output into tailed file

```text
Either by wrapping main (closing stdout and opening it for the tailed file) or wrapping printf
```

[ ] Add better solution for program end
