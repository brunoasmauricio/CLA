#! /bin/bash

# Exit on error
set -e
# Print all executed commands
set -x

CC=gcc

CC_FLAGS="-O0 -g3               \
         -Wl,-wrap=malloc       \
         -Wl,-wrap=free         \
         -Wl,-wrap=calloc       \
         -Wl,-wrap=realloc      \
         -Wl,-wrap=reallocarray \
         stdlib.o               \
         "

DBG=gdb

# This script:
# 1. Truncates/Creates an empty file used for gdb->tail communication
# 2. Compiles the provided source file with appropriate debug flags
# 3. Launch a separate 
# 4. Launch debugger for 

#1.
temporary_dir=$(mktemp -d)
fifo_path=$temporary_dir/CLA

touch $fifo_path
truncate --size 0 $fifo_path

# 2.
$CC $CC_FLAGS $# -o traced_binary.o

# 3.
urxvt -e tail -f $fifo_path &

# 4.
$DBG -iex 'set pagination off' \
    -ex 'set $fifo_path="'$fifo_path'"' \
    -ex 'source internals/setup.py' \
    traced_binary.o

# End script, reset state
set +e

kill %%
rm -f traced_binary.o
rm -f memory
rm -rf $temporary_dir